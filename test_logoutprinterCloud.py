#!/usr/bin/env python3
import pytest
import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class TestLogoutprinterCloud():
  def setup_method(self, method):
    self.driver = webdriver.Firefox()
    self.vars = {}
  
  def teardown_method(self, method):
    self.driver.quit()
  
  def test_logoutprinterCloud(self):
    self.driver.get("https://oscarsarabia.printercloud.com/admin/index.php")
    self.driver.set_window_size(841, 743)
    self.driver.find_element(By.ID, "user-menu").click()
    self.driver.find_element(By.LINK_TEXT, "Log Out").click()
    assert self.driver.find_element(By.ID, "forgot-password").text == "Lost Password"
  
